using System.Security.Permissions;

namespace ZTech.ViewModels {
    [PrincipalPermission(SecurityAction.Demand, Role = "Dispatcher")]
    [PrincipalPermission(SecurityAction.Demand, Role = "Manager")]
    public class DispatcherViewModel : Caliburn.Micro.PropertyChangedBase
    {
        public DispatcherViewModel()
        {
            AppTitle = "Dispatcher";
        }

        public string AppTitle { get; set; }
    }
}