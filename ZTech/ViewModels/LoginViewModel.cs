﻿using System;
using System.Threading;
using System.Windows.Controls;
using Caliburn.Micro;
using Contract.Security;
using Contract.Services;

namespace ZTech.ViewModels
{
    public class LoginViewModel : Screen
    {
        private readonly IAuthenticationService _authService;
        private string _status;

        public LoginViewModel(IAuthenticationService authService)
        {
            _authService = authService;
            IsLoginValid = false;
            Status = string.Empty;
            AppTitle = "Login";
        }

        public bool IsLoginValid { get; private set; }
        public string AppTitle { get; set; }
        public string Username { get; set; }

        public string Password { get; set; }

        public string Status
        {
            get { return _status; }
            set
            {
                _status = value;
                NotifyOfPropertyChange(() => Status);
            }
        }

        public void Login()
        {
            Status = string.Empty;
            if (CheckCredentials())
            {
                IsLoginValid = true;
                TryClose(true);
            }
        }

        public void Cancel()
        {
            IsLoginValid = false;
            TryClose(true);
        }

        public void PasswordChanged(Control control)
        {
            if (!(control is PasswordBox)) return;
            Password = (control as PasswordBox).Password;
        }

        private bool CheckCredentials()
        {
            try
            {
                if (string.IsNullOrWhiteSpace(Username) || string.IsNullOrWhiteSpace(Password))
                {
                    Status = "Login failed! Please provide some valid credentials.";
                    return false;
                }
                var user = _authService.AuthenticateUser(Username.Trim(), Password.Trim());

                var customPrincipal = (CustomPrincipal) Thread.CurrentPrincipal;

                customPrincipal.Identity = new CustomIdentity(user);

                return true;
            }
            catch (UnauthorizedAccessException)
            {
                Status = "Login failed! Please provide some valid credentials.";
            }
            catch (Exception ex)
            {
                Status = string.Format("ERROR: {0}", ex.Message);
            }
            return false;
        }
    }
}