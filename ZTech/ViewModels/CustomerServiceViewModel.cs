using System.Security.Permissions;

namespace ZTech.ViewModels
{
    [PrincipalPermission(SecurityAction.Demand, Role = "CustomerService")]
    [PrincipalPermission(SecurityAction.Demand, Role = "Manager")]
    public class CustomerServiceViewModel : Caliburn.Micro.PropertyChangedBase
    {
        public CustomerServiceViewModel()
        {
            AppTitle = "Customer Service";
        }

        public string AppTitle { get; set; }
    }
}