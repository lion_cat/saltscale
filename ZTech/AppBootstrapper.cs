using System.Threading;
using System.Windows;
using BusinessLogic.Security;
using Contract.Security;
using Contract.Services;
using ZTech.ViewModels;

namespace ZTech {
    using System;
    using System.Collections.Generic;
    using Caliburn.Micro;

    public class AppBootstrapper : BootstrapperBase {
        SimpleContainer _container;

        public AppBootstrapper() {
            Initialize();
        }

        protected override void Configure() {
            _container = new SimpleContainer();

            _container.Singleton<IWindowManager, WindowManager>();
            _container.Singleton<IEventAggregator, EventAggregator>();

            #region ViewModels
            _container.PerRequest<DispatcherViewModel>();
            _container.PerRequest<CustomerServiceViewModel>();
            _container.PerRequest<LoginViewModel>();
            #endregion

            #region Services
            _container.Singleton<IAuthenticationService, AuthenticationService>();
            #endregion
        }

        protected override object GetInstance(Type service, string key) {
            var instance = _container.GetInstance(service, key);
            if (instance != null)
                return instance;

            throw new InvalidOperationException("Could not locate any instances.");
        }

        protected override IEnumerable<object> GetAllInstances(Type service) {
            return _container.GetAllInstances(service);
        }

        protected override void BuildUp(object instance) {
            _container.BuildUp(instance);
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            var customPrincipal = new CustomPrincipal();
            AppDomain.CurrentDomain.SetThreadPrincipal(customPrincipal);

            if (StartLogin())
            {
                StartMainApplication();
            }

            Application.Shutdown();
        }

        private void StartMainApplication()
        {
            var customPrincipal = (CustomPrincipal)Thread.CurrentPrincipal;

            var identity = customPrincipal.Identity;
            if (identity.MainScreen == CustomIdentity.DispatcherScreen)
            {
                var shell = IoC.Get<DispatcherViewModel>();
                var windowManager = IoC.Get<IWindowManager>();
                windowManager.ShowDialog(shell);
            }
            else if (identity.MainScreen == CustomIdentity.CustomerServiceScreen)
            {
                var shell = IoC.Get<CustomerServiceViewModel>();
                var windowManager = IoC.Get<IWindowManager>();
                windowManager.ShowDialog(shell);
            }
        }

        bool StartLogin()
        {
            var login = IoC.Get<LoginViewModel>();
            var windowManager = IoC.Get<IWindowManager>();
            Application.ShutdownMode = ShutdownMode.OnExplicitShutdown;
            windowManager.ShowDialog(login);
            return login.IsLoginValid;
        }
    }
}