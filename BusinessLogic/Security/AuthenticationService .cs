﻿using System;
using System.Collections.Generic;
using System.Linq;
using Contract.Enums;
using Contract.Security;
using Contract.Services;
using DataAccess;

namespace BusinessLogic.Security
{
    public class AuthenticationService : IAuthenticationService
    {
        public IUser AuthenticateUser(string username, string clearTextPassword)
        {
            using (var adminData = new SaltScaleAdminDevEntities())
            {
                var user = (from u in adminData.Users.AsNoTracking()
                    join r in adminData.Roles.AsNoTracking() on u.UserRole equals r.RoleID
                    where u.UserName == username && u.UserPwd == clearTextPassword
                    select new {u.UserName, r.RoleLabel, r.RoleID, u.StartUpScreen}).SingleOrDefault();

                if (user == null)
                    throw new UnauthorizedAccessException("Access denied. Please provide some valid credentials.");

                List<string> allowedOperations = (from r in adminData.RoleOperations.AsNoTracking()
                    join o in adminData.Operations.AsNoTracking() on r.OperationID equals o.operationID
                    where r.RoleID == user.RoleID
                    select o.operationLabel).ToList();

                var permissions = new List<Permissions>();
                
                foreach (var item in allowedOperations)
                {
                    Permissions permission;

                    if (Enum.TryParse(item, out permission))
                    {
                        permissions.Add(permission);
                    }
                }

                var userData = new InternalUserData
                {
                    Username = user.UserName,
                    Role = user.RoleLabel,
                    MainScreen = user.StartUpScreen,
                    Operations = permissions
                };
                return userData;
            }
        }

        private class InternalUserData : IUser
        {
            public string Username { get; set; }

            public string Role { get; set; }

            public IReadOnlyList<Permissions> Operations { get; set; }
            public string MainScreen { get; set; }
        }
    }
}