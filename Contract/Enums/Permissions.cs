﻿namespace Contract.Enums
{
    public enum Permissions
    {
        Unknown,
        OrderEdit,
        CustomerEdit,
        TruckCreate,
        TruckEdit,
        TruckDelete,
        CustomerCreate,
        CustomerDelete,
        OrderCreate,
        POCreate,
        POEdit,
        PODelete,
        MaterialEdit,
        MaterialCreate,
        MaterialDelete,
        OrderDelete,
        FinancialExport,
        Archive,
        EditUsers,
        EditTruckRate,
    }
}