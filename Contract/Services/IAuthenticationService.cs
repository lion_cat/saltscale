﻿using Contract.Security;

namespace Contract.Services
{
    public interface IAuthenticationService
    {
        IUser AuthenticateUser(string username, string password);
    }
}