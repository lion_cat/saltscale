﻿using System.Collections.Generic;
using System.Security.Principal;
using Contract.Enums;

namespace Contract.Security
{
    public class CustomIdentity : IIdentity
    {
        public static string DispatcherScreen = "Dispatcher Screen";
        public static string CustomerServiceScreen = "CustomerService Screen";

        public CustomIdentity(string name, string role, List<Permissions> operations, string mainScreen)
        {
            Name = name;
            Role = role;
            AllowedOperations = operations;
            MainScreen = mainScreen;
        }

        public CustomIdentity(IUser user)
        {
            Name = user.Username;
            Role = user.Role;
            AllowedOperations = user.Operations;
            MainScreen = user.MainScreen;
        }

        public string MainScreen { get; private set; }
        public string Role { get; private set; }
        public IReadOnlyList<Permissions> AllowedOperations { get; private set; }

        #region IIdentity Members

        public string AuthenticationType
        {
            get { return "Custom authentication"; }
        }

        public bool IsAuthenticated
        {
            get { return !string.IsNullOrEmpty(Name); }
        }

        #endregion

        public string Name { get; private set; }
    }
}