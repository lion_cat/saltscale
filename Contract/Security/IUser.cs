﻿using System.Collections.Generic;
using Contract.Enums;

namespace Contract.Security
{
    public interface IUser
    {
        string Username { get; }

        string Role { get; }

        IReadOnlyList<Permissions> Operations { get; }

        string MainScreen { get; }
    }
}