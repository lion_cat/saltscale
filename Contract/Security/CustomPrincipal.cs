﻿using System;
using System.Security.Principal;

namespace Contract.Security
{
    public class CustomPrincipal : IPrincipal
    {
        private CustomIdentity _identity;

        public CustomIdentity Identity
        {
            get { return _identity ?? new AnonymousIdentity(); }
            set { _identity = value; }
        }

        #region IPrincipal Members
        IIdentity IPrincipal.Identity
        {
            get { return Identity; }
        }

        public bool IsInRole(string role)
        {
            return !string.IsNullOrWhiteSpace(_identity.Role) && _identity.Role.Equals(role, StringComparison.InvariantCultureIgnoreCase);
        }
        #endregion
    }
}