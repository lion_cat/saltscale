﻿using System.Collections.Generic;
using Contract.Enums;

namespace Contract.Security
{
    public class AnonymousIdentity : CustomIdentity
    {
        public AnonymousIdentity()
            : base(string.Empty, string.Empty, new List<Permissions>(), string.Empty)
        { }
    }
}