@echo off

SET DIR=%~d0%~p0%

SET database.name="SaltScaleRukert"
SET sql.files.directory="%DIR%..\db\SQLServer\ZTech"
SET server.database="localhost"
SET timeout.command="900"
SET timeout.admin="600"
SET outdir="%DIR%..\outDir"


"%DIR%Console\rh.exe" /d=%database.name% /f=%sql.files.directory% /s=%server.database% /o:%outdir% /ct=%timeout.command% /cta=%timeout.admin%

pause