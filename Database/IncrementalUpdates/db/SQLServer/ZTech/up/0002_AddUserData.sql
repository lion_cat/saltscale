BEGIN TRANSACTION
GO

SET IDENTITY_INSERT dbo.[User] ON;
GO

INSERT INTO dbo.[User] ([UserID], [UserName], [UserPwd] ,[CreateTime],[CAuditID],[UpdateTime],[UAuditID],[IsActive])
VALUES (99999, 'SYSTEM', '_', GETDATE(), 99999, NULL, NULL, 0);
INSERT INTO dbo.[User] ([UserID], [UserName], [UserPwd] ,[CreateTime],[CAuditID],[UpdateTime],[UAuditID],[IsActive])
VALUES (7, 'Dispatcher', 'Dispatcher', GETDATE(), 99999, NULL, NULL, 0);
INSERT INTO dbo.[User] ([UserID], [UserName], [UserPwd] ,[CreateTime],[CAuditID],[UpdateTime],[UAuditID],[IsActive])
VALUES (8, 'Developer', 'Developer', GETDATE(), 99999, NULL, NULL, 0);
INSERT INTO dbo.[User] ([UserID], [UserName], [UserPwd] ,[CreateTime],[CAuditID],[UpdateTime],[UAuditID],[IsActive])
VALUES (15, 'Rukert', 'Dispatcher', GETDATE(), 99999, NULL, NULL, 0);

GO

SET IDENTITY_INSERT dbo.[User] OFF;
GO
COMMIT
GO