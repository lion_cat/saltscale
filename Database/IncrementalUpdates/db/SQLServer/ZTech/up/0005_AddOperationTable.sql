BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO

CREATE TABLE [dbo].[Operation](
	[OperationID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[CreateTime] [datetime] NOT NULL,
	[CAuditID] [int] NOT NULL,
	[UpdateTime] [datetime] NULL,
	[UAuditID] [int] NULL,
	[IsActive] [bit] NOT NULL
 CONSTRAINT [PK_Operation] PRIMARY KEY CLUSTERED 
(
	[OperationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[Operation] ADD  CONSTRAINT [DF_Operation_CreateTime]  DEFAULT (getdate()) FOR [CreateTime]
GO

ALTER TABLE [dbo].[Operation] ADD  CONSTRAINT [DF_Operation_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO

ALTER TABLE dbo.[Operation] ADD CONSTRAINT
	FK_Operation_CreatedByUser FOREIGN KEY
	(
	CAuditID
	) REFERENCES dbo.[User]
	(
	UserID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.[Operation] ADD CONSTRAINT
	FK_Operation_UpdatedByUser FOREIGN KEY
	(
	UAuditID
	) REFERENCES dbo.[User]
	(
	UserID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
GO