BEGIN TRANSACTION
GO

SET IDENTITY_INSERT dbo.[Role] ON;
GO

INSERT [dbo].[Role] ([RoleID], [Name],[CreateTime],[CAuditID],[UpdateTime],[UAuditID],[IsActive]) 
VALUES (1, N'Developer', GETDATE(), 99999, NULL, NULL, 1)
GO
INSERT [dbo].[Role] ([RoleID], [Name],[CreateTime],[CAuditID],[UpdateTime],[UAuditID],[IsActive])
VALUES (2, N'Administrator', GETDATE(), 99999, NULL, NULL, 1)
GO
INSERT [dbo].[Role] ([RoleID], [Name],[CreateTime],[CAuditID],[UpdateTime],[UAuditID],[IsActive]) 
VALUES (3, N'Manager', GETDATE(), 99999, NULL, NULL, 1)
GO
INSERT [dbo].[Role] ([RoleID], [Name],[CreateTime],[CAuditID],[UpdateTime],[UAuditID],[IsActive]) 
VALUES (4, N'Office', GETDATE(), 99999, NULL, NULL, 1)
GO
INSERT [dbo].[Role] ([RoleID], [Name],[CreateTime],[CAuditID],[UpdateTime],[UAuditID],[IsActive]) 
VALUES (5, N'Dispatcher', GETDATE(), 99999, NULL, NULL, 1)
GO

SET IDENTITY_INSERT dbo.[Role] OFF;
GO
COMMIT
GO