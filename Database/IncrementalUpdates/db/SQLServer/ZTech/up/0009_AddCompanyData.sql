BEGIN TRANSACTION
GO
SET IDENTITY_INSERT dbo.[Company] ON;
GO

INSERT [dbo].[Company] ([CompanyID], [Name],[CreateTime],[CAuditID],[UpdateTime],[UAuditID],[IsActive]) 
VALUES (3, N'Eastern Salt', GETDATE(), 99999, NULL, NULL, 1)
GO

SET IDENTITY_INSERT dbo.[Company] OFF;
GO
COMMIT
GO