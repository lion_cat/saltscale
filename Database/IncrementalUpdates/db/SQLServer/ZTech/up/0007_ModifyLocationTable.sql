BEGIN TRANSACTION
GO
EXECUTE sp_rename N'dbo.Location.FullName', N'Name', 'COLUMN'; 
GO
ALTER TABLE dbo.Location ADD
	[CreateTime] [datetime] NULL,
	[CAuditID] [int] NULL,
	[UpdateTime] [datetime] NULL,
	[UAuditID] [int] NULL,
	[IsActive] [bit] NULL
GO
ALTER TABLE [dbo].[Location] ADD  CONSTRAINT [DF_Location_CreateTime]  DEFAULT (getdate()) FOR [CreateTime]
GO

ALTER TABLE [dbo].[Location] ADD  CONSTRAINT [DF_Location_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO

UPDATE dbo.Location SET CAuditID = 99999,  CreateTime=GETDATE(), IsActive=1;
GO
ALTER TABLE [dbo].[Location] ALTER COLUMN [Name] varchar(50) NOT NULL;
GO
ALTER TABLE [dbo].[Location] ALTER COLUMN [CreateTime] datetime NOT NULL;
GO
ALTER TABLE [dbo].[Location] ALTER COLUMN [CAuditID] int NOT NULL;
GO
ALTER TABLE [dbo].[Location] ALTER COLUMN [IsActive] bit NOT NULL;
GO
ALTER TABLE dbo.Location ADD CONSTRAINT
	FK_Location_CreatedByUser FOREIGN KEY
	(
	CAuditID
	) REFERENCES dbo.[User]
	(
	UserID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Location ADD CONSTRAINT
	FK_Location_UpdatedByUser FOREIGN KEY
	(
	UAuditID
	) REFERENCES dbo.[User]
	(
	UserID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
GO