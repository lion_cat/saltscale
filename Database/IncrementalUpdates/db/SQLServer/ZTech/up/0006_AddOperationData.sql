BEGIN TRANSACTION
GO
SET IDENTITY_INSERT dbo.[Operation] ON;
GO
INSERT [dbo].[Operation] ([operationID], [Name],[CreateTime],[CAuditID],[UpdateTime],[UAuditID],[IsActive]) VALUES (10, N'OrderEdit', GETDATE(), 99999, NULL, NULL, 1)
INSERT [dbo].[Operation] ([operationID], [Name],[CreateTime],[CAuditID],[UpdateTime],[UAuditID],[IsActive]) VALUES (11, N'CustomerEdit', GETDATE(), 99999, NULL, NULL, 1)
INSERT [dbo].[Operation] ([operationID], [Name],[CreateTime],[CAuditID],[UpdateTime],[UAuditID],[IsActive]) VALUES (12, N'TruckCreate', GETDATE(), 99999, NULL, NULL, 1)
INSERT [dbo].[Operation] ([operationID], [Name],[CreateTime],[CAuditID],[UpdateTime],[UAuditID],[IsActive]) VALUES (13, N'TruckEdit', GETDATE(), 99999, NULL, NULL, 1)
INSERT [dbo].[Operation] ([operationID], [Name],[CreateTime],[CAuditID],[UpdateTime],[UAuditID],[IsActive]) VALUES (14, N'TruckDelete', GETDATE(), 99999, NULL, NULL, 1)
INSERT [dbo].[Operation] ([operationID], [Name],[CreateTime],[CAuditID],[UpdateTime],[UAuditID],[IsActive]) VALUES (15, N'CustomerCreate', GETDATE(), 99999, NULL, NULL, 1)
INSERT [dbo].[Operation] ([operationID], [Name],[CreateTime],[CAuditID],[UpdateTime],[UAuditID],[IsActive]) VALUES (16, N'CustomerDelete', GETDATE(), 99999, NULL, NULL, 1)
INSERT [dbo].[Operation] ([operationID], [Name],[CreateTime],[CAuditID],[UpdateTime],[UAuditID],[IsActive]) VALUES (17, N'OrderCreate', GETDATE(), 99999, NULL, NULL, 1)
INSERT [dbo].[Operation] ([operationID], [Name],[CreateTime],[CAuditID],[UpdateTime],[UAuditID],[IsActive]) VALUES (18, N'POCreate', GETDATE(), 99999, NULL, NULL, 1)
INSERT [dbo].[Operation] ([operationID], [Name],[CreateTime],[CAuditID],[UpdateTime],[UAuditID],[IsActive]) VALUES (19, N'POEdit', GETDATE(), 99999, NULL, NULL, 1)
INSERT [dbo].[Operation] ([operationID], [Name],[CreateTime],[CAuditID],[UpdateTime],[UAuditID],[IsActive]) VALUES (20, N'PODelete', GETDATE(), 99999, NULL, NULL, 1)
INSERT [dbo].[Operation] ([operationID], [Name],[CreateTime],[CAuditID],[UpdateTime],[UAuditID],[IsActive]) VALUES (21, N'MaterialEdit', GETDATE(), 99999, NULL, NULL, 1)
INSERT [dbo].[Operation] ([operationID], [Name],[CreateTime],[CAuditID],[UpdateTime],[UAuditID],[IsActive]) VALUES (22, N'MaterialCreate', GETDATE(), 99999, NULL, NULL, 1)
INSERT [dbo].[Operation] ([operationID], [Name],[CreateTime],[CAuditID],[UpdateTime],[UAuditID],[IsActive]) VALUES (23, N'MaterialDelete', GETDATE(), 99999, NULL, NULL, 1)
INSERT [dbo].[Operation] ([operationID], [Name],[CreateTime],[CAuditID],[UpdateTime],[UAuditID],[IsActive]) VALUES (24, N'OrderDelete', GETDATE(), 99999, NULL, NULL, 1)
INSERT [dbo].[Operation] ([operationID], [Name],[CreateTime],[CAuditID],[UpdateTime],[UAuditID],[IsActive]) VALUES (25, N'EditUsers', GETDATE(), 99999, NULL, NULL, 1)
INSERT [dbo].[Operation] ([operationID], [Name],[CreateTime],[CAuditID],[UpdateTime],[UAuditID],[IsActive]) VALUES (26, N'EditTruckRate', GETDATE(), 99999, NULL, NULL, 1)
INSERT [dbo].[Operation] ([operationID], [Name],[CreateTime],[CAuditID],[UpdateTime],[UAuditID],[IsActive]) VALUES (27, N'Archive', GETDATE(), 99999, NULL, NULL, 1)
INSERT [dbo].[Operation] ([operationID], [Name],[CreateTime],[CAuditID],[UpdateTime],[UAuditID],[IsActive]) VALUES (28, N'FinancialExport', GETDATE(), 99999, NULL, NULL, 1)
GO

SET IDENTITY_INSERT dbo.[Operation] OFF;
GO
COMMIT
GO