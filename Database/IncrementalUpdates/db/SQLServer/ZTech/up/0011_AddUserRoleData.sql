BEGIN TRANSACTION
GO
SET IDENTITY_INSERT dbo.[UserRole] ON;
GO

INSERT INTO dbo.[UserRole] ([UserRoleID], [UserID], [RoleId])
VALUES (1, 7, 5);
INSERT INTO dbo.[UserRole] ([UserRoleID], [UserID], [RoleId])
VALUES (2, 8, 1);
INSERT INTO dbo.[UserRole] ([UserRoleID], [UserID], [RoleId] )
VALUES (3, 15, 3);

GO

SET IDENTITY_INSERT dbo.[UserRole] OFF;
GO
COMMIT
GO