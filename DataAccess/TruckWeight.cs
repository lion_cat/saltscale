//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class TruckWeight
    {
        public int TruckWeightID { get; set; }
        public int TruckID { get; set; }
        public int EmptyWeight { get; set; }
        public bool isManual { get; set; }
        public int CommonID { get; set; }
    }
}
