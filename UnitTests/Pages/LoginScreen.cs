﻿using TestStack.White.ScreenObjects;
using TestStack.White.UIItems;
using TestStack.White.UIItems.WindowItems;

namespace UnitTests.Pages
{
    public partial class LoginScreen : AppScreen
    {
        protected TextBox Username;
        protected TextBox Password;
        protected Button Login;
        protected Button Exit;
        public const string Title="Login";

        public LoginScreen(Window window, ScreenRepository screenRepository) : base(window, screenRepository) { }

        public virtual void FillForm(string username, string password)
        {
            Username.BulkText = username;
            Password.BulkText = password;
        }

        public virtual void PressLogin()
        {
            Login.Click();
        }
        public virtual void PressExit()
        {
            Exit.Click();
        }
    }
}